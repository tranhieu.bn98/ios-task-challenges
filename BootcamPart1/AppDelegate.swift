//
//  AppDelegate.swift
//  BootcamPart1
//
//  Created by TranHieu on 05/10/2023.
//

import UIKit
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyBjqBdt1hUm8aBcfZs4NdG1_7O6NbLjCuY")
      //  GMSPlacesClient.provideAPIKey("AIzaSyBjqBdt1hUm8aBcfZs4NdG1_7O6NbLjCuY")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pickColorVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as UIViewController
        let navigationController = UINavigationController(rootViewController: pickColorVC)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        if let window = window {
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
        }
        return true
    }
}

