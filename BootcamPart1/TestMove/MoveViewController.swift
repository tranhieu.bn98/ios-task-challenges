//
//  MoveViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 24/11/2023.
//

import UIKit

class MoveViewController: UIViewController {
    
    @IBOutlet weak var imgQuestion: UIImageView!
    
    @IBOutlet weak var imgResult: UIImageView!
    
    var vAnswer: AnswerView?
    var aspect: CGFloat = 0
    private let viewBg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.translatesAutoresizingMaskIntoConstraints = false
        imgQuestion.addSubview(viewBg)
        
        NSLayoutConstraint.activate([
            viewBg.topAnchor.constraint(equalTo: imgQuestion.topAnchor),
            viewBg.bottomAnchor.constraint(equalTo: imgQuestion.bottomAnchor),
            viewBg.leadingAnchor.constraint(equalTo: imgQuestion.leadingAnchor),
            viewBg.trailingAnchor.constraint(equalTo: imgQuestion.trailingAnchor)
        ])
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func addViewCrop(_ sender: Any) {
        
        //        let wImage = imgQuestion.frame.width - 50
        //        let hImage = imgQuestion.bounds.height
        //
        //        let xImage = imgQuestion.frame.origin.x
        //        let yImage = imgQuestion.frame.origin.y
        let viewAnswer = AnswerView()
        let height = imgQuestion.frame.height
        let width = imgQuestion.frame.width
        aspect = height/width
        print("imgQuestion.frame.width \(height)---- screen \(UIScreen.main.bounds.height)")
        viewAnswer.isUserInteractionEnabled = true
        
        viewAnswer.frame = CGRect(x: 30, y: 10, width: 100, height: 100)
        
        viewAnswer.delegate = self
        viewAnswer.widthOutSide = width
        viewAnswer.heightOutSide = height
        vAnswer = viewAnswer
        imgQuestion.addSubview(viewAnswer)
        imgQuestion.layer.layoutIfNeeded()
    }
    
    func addMarkForAnswerSelect(rect: CGRect) {
        // viewBg.layer.sublayers?.removeAll()
        
        let path = CGMutablePath()
        path.addRoundedRect(in: rect, cornerWidth: 0, cornerHeight: 0)
        path.closeSubpath()
        
        path.addRect(CGRect(origin: .zero, size: imgQuestion.frame.size))
        
        let maskLayer = CAShapeLayer()
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.path = path
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        
        viewBg.layer.mask = maskLayer
        viewBg.clipsToBounds = true
    }
    
    func croppedImage(rect: CGRect) -> UIImage? {
        var cropRect = rect
        var  imageSize: CGSize?
        imageSize = imgQuestion.image?.size
        var drawRect: CGRect = CGRect.zero
        
        let height = imgQuestion.frame.height
        let width = imgQuestion.frame.width
        
        let imageFrameW = rect.width
        
        let imageFrameH = rect.height
        
        let hAsepct: CGFloat = height / imageFrameH
        
        let wAsepct: CGFloat = width / imageFrameW
        
        aspect = hAsepct/wAsepct
        
        drawRect.size = imageSize!
        
        drawRect.origin.x = round(-cropRect.origin.x)
        drawRect.origin.y = round(-cropRect.origin.y)
        
        cropRect.size.width = round(cropRect.size.width / aspect)
        cropRect.size.height = round(cropRect.size.height / aspect)
        
        cropRect.origin.x = round(cropRect.origin.x)
        cropRect.origin.y = round(cropRect.origin.y)
        
        UIGraphicsBeginImageContextWithOptions(cropRect.size, true, 0)
        imgQuestion.image?.draw(in: drawRect)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return result
        
    }
    
    
    
}

extension MoveViewController: AnswerViewDelegate {
    func didEndPanPosition(position: CGRect, tag: Int, view: AnswerView) {
        addMarkForAnswerSelect(rect: position)
        
        
        
    }
    
    func didClickItem(position: CGRect, tag: Int, view: AnswerView) {
        
        vAnswer?.isSelectAnswer = true
        vAnswer?.updateUISelectAnswer()
        addMarkForAnswerSelect(rect: position)
        
        let height = imgQuestion.frame.height
        let width = imgQuestion.frame.width
        
                let imageFrameW = position.width
                let imageFrameH = position.height
        
                let x = position.origin.x * (width / imageFrameW)
        
                let y = position.origin.y * height / imageFrameH
        
                let widthCrop = position.width * width / imageFrameW
        
                let heightCrop = position.height * height / imageFrameH
        
                let croppedRect = CGRect(x: x, y: y, width: widthCrop, height: heightCrop)
        
                if let imageCrop = imgQuestion.image?.cgImage?.cropping(to: croppedRect){
                    imgResult.image = UIImage(cgImage: imageCrop)
                } else {
                    print("fail")
                }
//        let img = croppedImage(rect: position)
//        imgResult.contentMode = .center
//        imgResult.image = img
        
    }
    
    func didPanPosition(position: CGRect, tag: Int, view: AnswerView) {
        addMarkForAnswerSelect(rect: position)
        
    }
    
    
}
extension UIView {
    
    /// Create image snapshot of view.
    func snapshot(of rect: CGRect? = nil) -> UIImage {
        return UIGraphicsImageRenderer(bounds: rect ?? bounds).image { _ in
            drawHierarchy(in: bounds, afterScreenUpdates: true)
        }
    }
}
