//
//  ViewAnswer.swift
//  BootcamPart1
//
//  Created by TranHieu on 24/11/2023.
//

import Foundation
import UIKit

protocol AnswerViewDelegate: AnyObject {
    func didClickItem(position: CGRect, tag: Int, view: AnswerView)
    func didPanPosition(position: CGRect, tag: Int, view: AnswerView)
    func didEndPanPosition(position: CGRect, tag: Int, view: AnswerView)
}

class AnswerView: UIView {
    var currentEdge: Edge = .none
    var touchStart = CGPoint.zero
    weak var delegate: AnswerViewDelegate?
    
    var heightOutSide: CGFloat = 0
    var widthOutSide: CGFloat = 0
    
    let heighEdge: CGFloat = 30
    let widthEdge: CGFloat = 3
    var tagView: Int = -1
    
    static var edgeSize: CGFloat = 44.0
    
    var isActiveEdit: Bool = true
    var isSelectAnswer: Bool = false
    
    enum Edge {
        case none
        case left
        case topLeft
        case top
        case topRight
        case right
        case bottomRight
        case bottom
        case bottomLeft
    }
    
    private lazy var viewBorderAnwer: UIView = {
        let vAnswer = UIView()
        vAnswer.layer.borderWidth = 2
        vAnswer.layer.borderColor = UIColor.black.cgColor
        vAnswer.translatesAutoresizingMaskIntoConstraints = false
        vAnswer.isUserInteractionEnabled = true
        vAnswer.clipsToBounds = false
        vAnswer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickViewAnswer)))
        //   vAnswer.backgroundColor = UIColor.lightGray
        return vAnswer
    }()
    
    private lazy var lbNumberAnswer: UILabel = {
        let lbNumber = UILabel()
        lbNumber.textColor = .white
        lbNumber.backgroundColor = .orange
        lbNumber.layer.cornerRadius = 8
        lbNumber.textAlignment = .center
        lbNumber.clipsToBounds = true
        lbNumber.text = "1"
        lbNumber.font = UIFont.systemFont(ofSize: 10)
        lbNumber.translatesAutoresizingMaskIntoConstraints = false
        return lbNumber
    }()
    
    private lazy var viewTopLeft1: UIView = {
        let view1 = UIView()
        view1.layer.backgroundColor = UIColor.orange.cgColor
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.clipsToBounds = true
        return view1
    }()
    
    private lazy var viewTopLeft2: UIView = {
        let view2 = UIView()
        view2.layer.backgroundColor = UIColor.orange.cgColor
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.clipsToBounds = true
        return view2
    }()
    
    private lazy var viewTopRight1: UIView = {
        let view1 = UIView()
        view1.layer.backgroundColor = UIColor.orange.cgColor
        view1.translatesAutoresizingMaskIntoConstraints = false
        return view1
    }()
    
    private lazy var viewTopRight2: UIView = {
        let view2 = UIView()
        view2.layer.backgroundColor = UIColor.orange.cgColor
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.clipsToBounds = true
        return view2
    }()
    
    private lazy var viewBottomLeft1: UIView = {
        let view1 = UIView()
        view1.layer.backgroundColor = UIColor.orange.cgColor
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.clipsToBounds = true
        return view1
    }()
    
    private lazy var viewBottomLeft2: UIView = {
        let view2 = UIView()
        view2.layer.backgroundColor = UIColor.orange.cgColor
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.clipsToBounds = true
        return view2
    }()
    
    private lazy var viewBottomRight1: UIView = {
        let view1 = UIView()
        view1.layer.backgroundColor = UIColor.orange.cgColor
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.clipsToBounds = true
        return view1
    }()
    
    private lazy var viewBottomRight2: UIView = {
        let view2 = UIView()
        view2.layer.backgroundColor = UIColor.orange.cgColor
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.clipsToBounds = true
        return view2
    }()
    let cropBoxHotArea: CGFloat = 100
    let cropBoxMinSize: CGFloat = 20
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func updateUISelectAnswer() {
        if isSelectAnswer {
            viewBorderAnwer.layer.borderColor = UIColor.orange.cgColor
            viewBorderAnwer.addSubview(viewTopLeft1)
            viewBorderAnwer.addSubview(viewTopLeft2)
            
            NSLayoutConstraint.activate([
                viewTopLeft1.topAnchor.constraint(equalTo: self.topAnchor),
                viewTopLeft1.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
                viewTopLeft1.heightAnchor.constraint(equalToConstant: 3),
                viewTopLeft1.widthAnchor.constraint(equalToConstant: 10)
            ])
            
            NSLayoutConstraint.activate([
                viewTopLeft2.topAnchor.constraint(equalTo: self.topAnchor),
                viewTopLeft2.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
                viewTopLeft2.heightAnchor.constraint(equalToConstant: 10),
                viewTopLeft2.widthAnchor.constraint(equalToConstant: 3)
            ])
            
            viewBorderAnwer.addSubview(viewTopRight1)
            viewBorderAnwer.addSubview(viewTopRight2)
            
            NSLayoutConstraint.activate([
                viewTopRight1.topAnchor.constraint(equalTo: self.topAnchor),
                viewTopRight1.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
                viewTopRight1.heightAnchor.constraint(equalToConstant: 3),
                viewTopRight1.widthAnchor.constraint(equalToConstant: 10)
            ])
            
            NSLayoutConstraint.activate([
                viewTopRight2.topAnchor.constraint(equalTo: self.topAnchor),
                viewTopRight2.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
                viewTopRight2.heightAnchor.constraint(equalToConstant: 10),
                viewTopRight2.widthAnchor.constraint(equalToConstant: 3)
            ])
            
            
            
            viewBorderAnwer.addSubview(viewBottomLeft1)
            viewBorderAnwer.addSubview(viewBottomLeft2)
            
            
            NSLayoutConstraint.activate([
                viewBottomLeft1.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                viewBottomLeft1.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
                viewBottomLeft1.heightAnchor.constraint(equalToConstant: 3),
                viewBottomLeft1.widthAnchor.constraint(equalToConstant: 10)
            ])
            
            NSLayoutConstraint.activate([
                viewBottomLeft2.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                viewBottomLeft2.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
                viewBottomLeft2.heightAnchor.constraint(equalToConstant: 10),
                viewBottomLeft2.widthAnchor.constraint(equalToConstant: 3)
            ])
            
            
            
            viewBorderAnwer.addSubview(viewBottomRight1)
            viewBorderAnwer.addSubview(viewBottomRight2)
            
            NSLayoutConstraint.activate([
                viewBottomRight1.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                viewBottomRight1.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
                viewBottomRight1.heightAnchor.constraint(equalToConstant: 3),
                viewBottomRight1.widthAnchor.constraint(equalToConstant: 10)
            ])
            
            NSLayoutConstraint.activate([
                viewBottomRight2.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                viewBottomRight2.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
                viewBottomRight2.heightAnchor.constraint(equalToConstant: 10),
                viewBottomRight2.widthAnchor.constraint(equalToConstant: 3)
            ])
        }
    }
    
    private func setupView(){
        viewBorderAnwer.isUserInteractionEnabled = true
        viewBorderAnwer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewBorderAnwer)
        addSubview(lbNumberAnswer)
        
        NSLayoutConstraint.activate([
            viewBorderAnwer.topAnchor.constraint(equalTo: self.topAnchor, constant: 2),
            viewBorderAnwer.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -2),
            viewBorderAnwer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 2),
            viewBorderAnwer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -2)
            
        ])
        
        NSLayoutConstraint.activate([
            lbNumberAnswer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: -6),
            lbNumberAnswer.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            lbNumberAnswer.heightAnchor.constraint(equalToConstant: 16),
            lbNumberAnswer.widthAnchor.constraint(equalToConstant: 16)
            
        ])
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isActiveEdit {
            return
        }
        
        if let touch = touches.first {
            
            touchStart = touch.location(in: self)
            
            currentEdge = { if self.bounds.size.width - touchStart.x < Self.edgeSize && self.bounds.size.height - touchStart.y < Self.edgeSize {
                //BottomRight
                return .bottomRight
                
            } else if touchStart.x < Self.edgeSize && self.bounds.size.height - touchStart.y < Self.edgeSize {
                //BottomLeft
                
                return .bottomLeft
            } else if (touchStart.x < Self.edgeSize && touchStart.y < Self.edgeSize) {
                //TopLeft
                
                return .topLeft
            } else if (self.bounds.size.width-touchStart.x < Self.edgeSize && touchStart.y < Self.edgeSize){
                //TopRight
                return .topRight
                
                
            } else if (self.bounds.size.height - touchStart.y < Self.edgeSize){
                //Bottom
                return .bottom
                
            } else if (touchStart.y < Self.edgeSize) {
                //Top
                return .top
                
            } else if (self.bounds.size.width - touchStart.x < Self.edgeSize) {
                //Right
                return .right
                
            } else if (touchStart.x < Self.edgeSize){
                //Left
                return .left
                
                
            }
                return .none
            }()
            
            print("touchesBegan \(currentEdge)")
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            
            let previous = touch.previousLocation(in: self)
            
            let originX = self.frame.origin.x
            let originY = self.frame.origin.y
            
            let width = self.frame.size.width
            let height = self.frame.size.height
            
            let deltaWidth = currentPoint.x - previous.x
            let deltaHeight = currentPoint.y - previous.y

            switch currentEdge {
            case .topLeft:
                self.frame = CGRect(x: originX + deltaWidth,
                                    y: originY + deltaHeight,
                                    width: width - deltaWidth < 20 ? 20 : width - deltaWidth,
                                    height: height - deltaHeight < 20 ? 20 : height - deltaHeight)
            case .topRight:
                self.frame = CGRect(x: originX,
                                    y: originY + deltaHeight,
                                    width: width + deltaWidth < 20 ? 20 : width + deltaWidth,
                                    height: height - deltaHeight < 20 ? 20 : height - deltaHeight)
                
            case .bottomRight:
                self.frame = CGRect(x: originX,
                                    y: originY,
                                    width: width + deltaWidth < 20 ? 20 : width + deltaWidth,
                                    height: height + deltaWidth < 20 ? 20 : height + deltaWidth)
            case .bottomLeft:
                self.frame = CGRect(x: originX + deltaWidth,
                                    y: originY,
                                    width: width - deltaWidth < 20 ? 20 : width - deltaWidth,
                                    height: height + deltaHeight < 20 ? 20 : height + deltaHeight)
            case .top:
                let newH = self.heightOutSide - originY - height
                
                self.frame = CGRect(x: originX,
                                    y: originY + deltaHeight < 0 ? 0 :  (originY + deltaHeight < 20 ? originY : originY + deltaHeight),
                                    width: width,
                                    height: height - deltaHeight > self.heightOutSide - newH ? self.heightOutSide - newH : (height - deltaHeight < 20 ? 20: height - deltaHeight ))
            case .bottom:
                self.frame = CGRect(x: originX,
                                    y: originY,
                                    width: width,
                                    height: height + deltaHeight > self.heightOutSide - originY ? self.heightOutSide - originY :  (height + deltaHeight < 20 ? 20 : height + deltaHeight))
                

            case .left:
                let newW = self.widthOutSide - originX - width
                self.frame = CGRect(x: originX + deltaWidth < 0 ? 0 : (width - deltaWidth < 20 ? originX : originX + deltaWidth),
                                    y: originY,
                                    width: width - deltaWidth > self.widthOutSide - newW ? self.widthOutSide - newW  :  (width - deltaWidth < 20 ? 20 :  width - deltaWidth),
                                    height: height)
            case .right:
                self.frame = CGRect(x: originX,
                                    y: originY,
                                    width: width + deltaWidth > self.widthOutSide - originX ? self.widthOutSide - originX : width + deltaWidth < 20 ? 20 : width + deltaWidth ,
                                    height: height)
            default:
                // Moving
                let positionX = self.center.x + currentPoint.x - touchStart.x
                let positionY = self.center.y + currentPoint.y - touchStart.y
                
                var newX: CGFloat = positionX
                var newY: CGFloat = positionY
                
                if positionX < (width / 2) {
                    newX = (width / 2)
                }
                if positionY < (height / 2) {
                    newY = (height / 2)
                }
                if positionX > self.widthOutSide - (width / 2){
                    newX = self.widthOutSide - (width / 2)
                }
                if positionY > self.heightOutSide - (height / 2){
                    newY = self.heightOutSide - (height / 2)
                }
                self.center = CGPoint(x: newX, y: newY)
            }
            
            self.delegate?.didPanPosition(position: self.frame, tag: self.tagView, view: self)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
   
        currentEdge = .none
        
        self.delegate?.didEndPanPosition(position: self.frame, tag: self.tagView, view: self)
    }
    
    @objc
    private func onClickViewAnswer(_ sender: UITapGestureRecognizer?){
        
        self.delegate?.didClickItem(position: self.frame, tag: self.tagView, view: self)
    }
}
