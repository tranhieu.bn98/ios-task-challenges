//
//  MapViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 25/10/2023.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapKit: MKMapView!
    
    @IBOutlet weak var collectionLocation: UICollectionView!
    
    var locationManager = CLLocationManager()
    
    var sourceLocation : CLLocationCoordinate2D?
    
    var lstLocation: [LocationItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        mapKit.delegate = self
        title = "MapKit"
        mapKit.showsUserLocation = true
        determineCurrentLocation()
        collectionLocation.delegate = self
        collectionLocation.dataSource = self
        collectionLocation.register(UINib(nibName: "LocationItemViewCell", bundle: nil), forCellWithReuseIdentifier: "LocationItemViewCell")
        
       
        
        let listLocation = LocationDataBase.shared.loadJson(filename: "location_data")
        lstLocation = listLocation ?? []
        listLocation?.forEach({ item in
            let cap = Capital(title: item.city ?? "nil", coordinate: CLLocationCoordinate2D(latitude: item.latitude, longitude: item.longitude), info: item.admin_name ?? "nil")
            mapKit.addAnnotation(cap)
        })
        collectionLocation.reloadData()
    }
    
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.startUpdatingLocation()
       
    }
    
    func setRoute(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        // Create source and destination placemark.
        let sourcePlaceMark = MKPlacemark(coordinate: source)
        let destinationPlaceMark = MKPlacemark(coordinate: destination)
        
        // Set direction request object.
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionRequest.transportType = .automobile
        
        // Make direction request object.
        let direction = MKDirections(request: directionRequest)
        direction.calculate { (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    
                    let alertController = UIAlertController(
                        title: "Title",
                        message: "Directions Not Available",
                        preferredStyle: UIAlertController.Style.alert
                    )
                    
                    let confirmAction = UIAlertAction(
                        title: "OK", style: UIAlertAction.Style.default) { (action) in
                        // ...
                    }

                    alertController.addAction(confirmAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    print("we have error getting directions==\(error.localizedDescription)")
                }
                return
            }
            
            for route in directionResonse.routes {
                
                self.mapKit.addOverlay(route.polyline)
                self.mapKit.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
        
        locationManager.stopUpdatingLocation()
    }
    
    
}

extension MapViewController: MKMapViewDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
         
        switch manager.authorizationStatus {
        case .authorizedAlways:
            print("author 1")
        case .notDetermined:
            print("autho 2")
        case .authorizedWhenInUse:
            
            
            print("author 3")
        case .restricted:
            print("author4")
        default:
            print("author5")
        }
        
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
       
        let overlay = overlay as? MKPolyline
        /// define a list of colors you want in your gradient
        let gradientColors = [UIColor.green, UIColor.blue, UIColor.red]
        
        /// Initialise a GradientPathRenderer with the colors
        let polylineRenderer = GradientPathRenderer(polyline: overlay!, colors: gradientColors)
        
        /// set a linewidth
        polylineRenderer.lineWidth = 10
        return polylineRenderer
        
    }
    
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            sourceLocation  = manager.location!.coordinate
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.mapKit.setRegion(region, animated: true)
        }
    }
}

extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lstLocation.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationItemViewCell", for: indexPath) as! LocationItemViewCell
        let item = lstLocation[indexPath.row]
        cell.updateUI(data: item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = collectionLocation.bounds.height
        let width: CGFloat = (UIScreen.main.bounds.width - 40) / 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\(indexPath.row)")
        mapKit.removeOverlays(mapKit.overlays)
        let location = lstLocation[indexPath.row]
        let center = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        setRoute(source: sourceLocation!, destination: center)
    }
}

