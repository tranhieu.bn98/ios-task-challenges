//
//  LocationItemViewCell.swift
//  BootcamPart1
//
//  Created by TranHieu on 27/10/2023.
//

import UIKit

class LocationItemViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var lbCity: UILabel!
    
    @IBOutlet weak var lbSubCity: UILabel!
    
    @IBOutlet weak var lbHour: UILabel!
    
    @IBOutlet weak var lbNumberKM: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.cornerRadius = 8
        imgLocation.layer.cornerRadius = 8
    }

    
    func updateUI(data: LocationItem) {
        lbCity.text = data.city ?? "nil"
        lbSubCity.text = data.admin_name ?? ""
    }
}
