//
//  LocationData.swift
//  BootcamPart1
//
//  Created by TranHieu on 26/10/2023.
//

import Foundation

class LocationDataBase {
    
    static let shared = LocationDataBase()
    
   
    
    func loadJson(filename fileName: String) -> [LocationItem]? {
    
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(LocationData.self, from: data)
                return jsonData.location
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
   
}
