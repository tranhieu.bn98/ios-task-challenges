//
//  LocationModel.swift
//  BootcamPart1
//
//  Created by TranHieu on 26/10/2023.
//

import Foundation
import MapKit
import CoreLocation

struct LocationData: Decodable {
    var location: [LocationItem]?
}

struct LocationItem: Decodable {
    var city: String?
    var lat: String?
    var lng: String?
    
    var country: String?
    var iso2: String?
    var admin_name: String?
    var capital: String?
    var population: String?
    var population_proper: String?
    
    var latitude: Double {
        let latDouble = Double(lat ?? "0") ?? 0
        return latDouble
    }
    
    
    var longitude: Double {
        let lngDouble = Double(lng ?? "0") ?? 0
        return lngDouble
    }
}

class Capital: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var info: String
    
    init(title: String, coordinate: CLLocationCoordinate2D, info: String) {
        self.title = title
        self.coordinate = coordinate
        self.info = info
    }
}
