//
//  ViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 05/10/2023.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionOpenMap(_ sender: Any) {
        goToOpenMap()
    }
    
    @IBAction func actionAnimation(_ sender: Any) {
        gotoDragAndDropViewController()
    }
    
    @IBAction func actionPokemonAR(_ sender: Any) {
        goToOpenMapPokemon()
    }
//    @IBAction func actionOpenLuckyWheel(_ sender: Any) {
//        goToOpenLuckyWheel()
//    }
//    
//    @IBAction func actionOpenTestMove(_ sender: Any) {
//        goToOpenTestMove()
//    }
    
    private func goToOpenMapPokemon() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "AddPokemonViewController", bundle:nil)
        let viewControllerB = storyBoard.instantiateViewController(withIdentifier: "AddPokemonViewController") as! AddPokemonViewController
        self.navigationController?.pushViewController(viewControllerB, animated: true)
    }
    
    private func goToOpenMap() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Map", bundle:nil)
        let viewControllerB = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(viewControllerB, animated: true)
    }
    
    private func goToOpenLuckyWheel() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "LuckyWheelController", bundle:nil)
        let viewControllerB = storyBoard.instantiateViewController(withIdentifier: "LuckyWheelController") as! LuckyWheelController
        self.navigationController?.pushViewController(viewControllerB, animated: true)
    }
    
    private func goToOpenTestMove() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "MoveViewController", bundle:nil)
        let viewControllerB = storyBoard.instantiateViewController(withIdentifier: "MoveViewController") as! MoveViewController
        self.navigationController?.pushViewController(viewControllerB, animated: true)
    }
    
    private func gotoDragAndDropViewController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "DragAndDropViewController", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DragAndDropViewController") as! DragAndDropViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
