//
//  LuckyWheelController.swift
//  BootcamPart1
//
//  Created by TranHieu on 30/10/2023.
//

import UIKit

class LuckyWheelController: UIViewController {
    
//    var wheel :LuckyWheel?
//    var items = [WheelItem]()
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        wheel = LuckyWheel(frame: CGRect(x: 0, y: 0, width: view.frame.width / 1.5 , height: 400))
//        wheel?.delegate = self
//        wheel?.dataSource = self
//        wheel?.center = self.view.center
//        wheel?.setTarget(section: 5)
//        wheel?.animateLanding = true
//        
//        self.view.addSubview(wheel!)
//    }
//    func numberOfSections() -> Int {
//        return 8
//    }
//    func itemsForSections() -> [WheelItem] {
//        items.append(WheelItem(title: "111", titleColor: UIColor.white, itemColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)))
//        items.append(WheelItem(title:  "222", titleColor: UIColor.white, itemColor: #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)))
//        items.append(WheelItem(title:  "333", titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), itemColor: #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)))
//        items.append(WheelItem(title:  "444", titleColor: UIColor.white, itemColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)))
//        items.append(WheelItem(title: "555", titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), itemColor: #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)))
//        items.append(WheelItem(title:  "666", titleColor: UIColor.white, itemColor: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)))
//        items.append(WheelItem(title:  "777", titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), itemColor: #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)))
//        items.append(WheelItem(title:  "888", titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), itemColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)))
//        return items
//    }
//    func wheelDidChangeValue(_ newValue: Int) {
//        print(newValue)
//    }
    var overlay: UIView = UIView()
       var overlay2: UIView = UIView()

       override func viewDidLoad() {
           super.viewDidLoad()
           
           self.view.backgroundColor = UIColor.gray
           // Do any additional setup after loading the view.
       }
       
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           
           overlay = createOverlay()
           self.view.addSubview(overlay)
           self.view.sendSubviewToBack(overlay)
       }
       
       func createOverlay() -> UIView {
           
           let overlayView = UIView(frame: view.frame)
           overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.4)

           let path = CGMutablePath()
           path.addRoundedRect(in: CGRect(x: 10, y: 30, width: 100, height: 200), cornerWidth: 5, cornerHeight: 5)
           
           path.addRoundedRect(in: CGRect(x: 15, y: overlayView.center.y + 120, width: overlayView.frame.width-30, height: 200), cornerWidth: 5, cornerHeight: 5)
           path.closeSubpath()
           
//           let shape = CAShapeLayer()
//           shape.path = path
//           shape.lineWidth = 3.0
//           shape.strokeColor = UIColor.blue.cgColor
//           shape.fillColor = UIColor.blue.cgColor
//           
//           overlayView.layer.addSublayer(shape)
//           
           path.addRect(CGRect(origin: .zero, size: overlayView.frame.size))

           let maskLayer = CAShapeLayer()
           maskLayer.backgroundColor = UIColor.black.cgColor
           maskLayer.path = path
           maskLayer.fillRule = CAShapeLayerFillRule.evenOdd

           overlayView.layer.mask = maskLayer
           overlayView.clipsToBounds = true
           
           return overlayView
       }

}
