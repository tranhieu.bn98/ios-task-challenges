//
//  WorkModel.swift
//  BootcamPart1
//
//  Created by TranHieu on 18/12/2023.
//

import Foundation

struct WorkModel {
    var time: String
    
    static func fakeData() -> [WorkModel] {
        let dataWork = [
            WorkModel(time: "08:00"),
            WorkModel(time: "09:00"),
            WorkModel(time: "10:00"),
            WorkModel(time: "11:00"),
            WorkModel(time: "12:00"),
            WorkModel(time: "13:00"),
            WorkModel(time: "14:00"),
            WorkModel(time: "15:00"),
            WorkModel(time: "16:00"),
            WorkModel(time: "17:00"),
            WorkModel(time: "18:00"),
            WorkModel(time: "19:00")]
        return dataWork
    }
}


public struct StaffModel {
    var name: String
    var avatar: String
    
    static func fakeData() -> [StaffModel] {
        let staff = [
            StaffModel(name: "Hà Linh", avatar: "girl_1"),
            StaffModel(name: "Cô giáo thảo", avatar: "girl_2"),
            StaffModel(name: "An Nhiên", avatar: "girl_3"),
            StaffModel(name: "Hoa Như", avatar: "girl_4"),
            StaffModel(name: "Bạch Trà My", avatar: "girl_5"),
            StaffModel(name: "Ngô Mỹ Duyên", avatar: "girl_6")
        ]
        
        return staff
    }
}
