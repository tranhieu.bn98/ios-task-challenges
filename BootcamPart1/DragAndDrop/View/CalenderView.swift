//
//  CalenderView.swift
//  BootcamPart1
//
//  Created by TranHieu on 11/12/2023.
//

import Foundation
import UIKit

public protocol CalenderViewDataSource: NSObject {
    func numberOfStaff() -> Int
    func numberOfTime(in column: Int) -> Int
}

public protocol CalenderViewDelegate: NSObject {
    func calenderView(titleForLeftRowAt index: Int) -> String
    
    func calenderView(column: RowsView, didSelectedColumnAt columnIndex: Int)
    func calenderView(titleForNameStaff index: Int) -> String
    
    func calenderView(avatarForNameStaff index: Int) -> String
    
    func calenderViewAlertSelectService()
    
    func calenderView(itemStaff index: Int) -> StaffModel
}

open class CalenderView: UIView {
    
    private var scrollViewCalender = UIScrollView()
    
    private var scrollViewTime = UIScrollView()
    private var scrollViewAvatar = UIScrollView()
    
    private var buttonAddAvatar = UIButton()
    
    
    private var lineTimeCurrent = UIView()
    
    private var lbTimeCurrent = UILabel()
    
    open var delegate: CalenderViewDelegate?
    open var dataSource: CalenderViewDataSource?
    
    var offsetIndexCollumn : [CGFloat] = []
    var offSetYRows: [CGFloat] = []
    var listWorkModel: [WorkModel] = []
    var offSetYRowsAtTime: [CGFloat] = []
    var listBookingView: [BookingView] = []
    
    
    var countdownTimer: Timer?
    
    private var totalTime = 60
    
    var isScrollEnabled: Bool = true {
        didSet {
            scrollViewCalender.isScrollEnabled = isScrollEnabled
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData() {
        setupData()
        setupColumns()
        showTime()
    }
    
    func reloadBookingView() {
        listBookingView.forEach { item in
            scrollViewCalender.addSubview(item)
        }
       // showTime()
    }
    
    func setupData() {
        offSetYRows.removeAll()
        offSetYRowsAtTime.removeAll()
        for i in 0..<listWorkModel.count{
            offSetYRows.append(CGFloat((i)) * Dimension.heightRow)
        }
        
        let data = listWorkModel.count * 5
        for i in 0..<data {
            offSetYRowsAtTime.append(CGFloat((i)) * (Dimension.heightRow / 5))
        }
        
    }
    
    private func startTimer() {
        DispatchQueue.main.async {
            self.countdownTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
                self.updateTime()
            })
        }
    }
    
    func updateTime() {
        print("time count \(totalTime)")
        if totalTime != 0 {
            totalTime -= 1
           
        } else {
            showTime()
            endTimer()
        }
    }
    
     func endTimer() {
        totalTime = 60
      // countdownTimer?.invalidate()
    }
    
    func setupView() {
        startTimer()
        
        scrollViewCalender.delegate = self
        scrollViewAvatar.delegate = self
        scrollViewTime.delegate = self
        buttonAddAvatar.layer.borderWidth = 1
        buttonAddAvatar.layer.borderColor = UIColor.lightGray.cgColor
        buttonAddAvatar.translatesAutoresizingMaskIntoConstraints = false
        
        scrollViewCalender.isUserInteractionEnabled = true
        
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.addLongPressBookingView))
        recognizer.minimumPressDuration = 1
        scrollViewCalender.addGestureRecognizer(recognizer)
        
        scrollViewAvatar.isScrollEnabled = false
        scrollViewTime.isScrollEnabled = false
        
        addSubview(buttonAddAvatar)
        buttonAddAvatar.setTitle("+", for: .normal)
        buttonAddAvatar.setTitleColor(.black, for: .normal)
        buttonAddAvatar.addTarget(self, action: #selector(addBookingView(_:)), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            buttonAddAvatar.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            buttonAddAvatar.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            buttonAddAvatar.widthAnchor.constraint(equalToConstant: Dimension.dimensionButton),
            buttonAddAvatar.heightAnchor.constraint(equalToConstant: Dimension.dimensionButton)
        ])
        
        scrollViewAvatar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollViewAvatar)
        
        NSLayoutConstraint.activate([
            scrollViewAvatar.topAnchor.constraint(equalTo: self.topAnchor),
            scrollViewAvatar.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollViewAvatar.leadingAnchor.constraint(equalTo: buttonAddAvatar.trailingAnchor),
            scrollViewAvatar.heightAnchor.constraint(equalToConstant: Dimension.heightAvatar)
        ])
        
        
        scrollViewTime.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollViewTime)
        
        NSLayoutConstraint.activate([
            scrollViewTime.topAnchor.constraint(equalTo: buttonAddAvatar.bottomAnchor),
            scrollViewTime.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            scrollViewTime.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            scrollViewTime.widthAnchor.constraint(equalToConstant: Dimension.widthTime)
        ])
        
        scrollViewCalender.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollViewCalender)
        
        NSLayoutConstraint.activate([
            scrollViewCalender.topAnchor.constraint(equalTo: scrollViewAvatar.bottomAnchor),
            scrollViewCalender.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollViewCalender.leadingAnchor.constraint(equalTo: scrollViewTime.trailingAnchor),
            scrollViewCalender.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func showTime() {
        scrollViewCalender.addSubview(lineTimeCurrent)
        lineTimeCurrent.isHidden = true
        lineTimeCurrent.backgroundColor = .red
        lineTimeCurrent.clipsToBounds = true
       
        scrollViewTime.addSubview(lbTimeCurrent)
        lbTimeCurrent.backgroundColor = .white
        lbTimeCurrent.clipsToBounds = true
        lbTimeCurrent.font = UIFont.systemFont(ofSize: 10)
        lbTimeCurrent.textAlignment = .center
        lbTimeCurrent.isHidden = true
        lbTimeCurrent.layer.cornerRadius = 12
        lbTimeCurrent.layer.borderColor = UIColor.red.cgColor
        lbTimeCurrent.layer.borderWidth = 1
        lbTimeCurrent.text = "08:00"
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let time = formatter.string(from: date)
        
        let timeArray = time.split(separator: ":")
        
        guard let hour = timeArray.first else { return  }
         
        guard let minues = timeArray.last else { return  }
        
        let hourInt = (Int(hour) ?? 8) - 8
        
        let minuesInt = (150 / 60) * (Int(minues) ?? 0)
        
        let positonTime = hourInt * 150
        
        let postionLeft = positonTime + minuesInt
         
    
        lineTimeCurrent.isHidden = false
        scrollViewCalender.bringSubviewToFront(lineTimeCurrent)
        lineTimeCurrent.frame = CGRect(x: 0, y: (postionLeft + 15), width: numberOfStaff() * 100, height: 1)
        
        lbTimeCurrent.isHidden = false
        scrollViewTime.bringSubviewToFront(lbTimeCurrent)
        lbTimeCurrent.frame = CGRect(x: 0, y: postionLeft, width: 50, height: 30)
        lbTimeCurrent.text = "\(hour):\(minues)"
        scrollViewTime.layoutIfNeeded()
        scrollViewTime.contentOffset.y = scrollViewCalender.contentOffset.y
    }

    // MARK: - setup UI Rows and Column
    func reloadItem(){
        scrollViewTime.subviews.forEach({$0.removeFromSuperview()})
        scrollViewAvatar.subviews.forEach({$0.removeFromSuperview()})
        scrollViewCalender.subviews.forEach({$0.removeFromSuperview()})
    }
    
    private func setupColumns(){
        
        var preview: UIView = RowsView()
        var previewLeft: UILabel = UILabel()
        var previewHeader: RowAvatarView = RowAvatarView()
        
        if numberOfStaff() > 0 {
            for i in 0..<numberOfRowInColumn(columnIndex: 0) {
                createColumnLeft(index: i, previewView: &previewLeft)
            }
        }
        offsetIndexCollumn.removeAll()
        for i in 0..<numberOfStaff() {
            let column = RowsView(columnIndex: i, numberOfRows: numberOfRowInColumn(columnIndex: i), heightRow: Dimension.heightRow)
            
            column.translatesAutoresizingMaskIntoConstraints = false
            
            scrollViewCalender.addSubview(column)
            
            let offsetX = CGFloat(i) * Dimension.withColumn
            offsetIndexCollumn.append(offsetX)
            
            NSLayoutConstraint.activate([
                column.topAnchor.constraint(equalTo: scrollViewCalender.topAnchor),
                column.bottomAnchor.constraint(equalTo: scrollViewCalender.bottomAnchor),
                
                column.widthAnchor.constraint(equalToConstant: Dimension.withColumn),
                column.heightAnchor.constraint(equalToConstant: caculateHeightColumn(columnIndex: i)),
            ])
                
            if i == 0 {
                NSLayoutConstraint.activate([
                column.leftAnchor.constraint(equalTo: scrollViewCalender.leftAnchor)
                ])
            } else {
                NSLayoutConstraint.activate([
                column.leftAnchor.constraint(equalTo: preview.rightAnchor)
                ])
            }
         
            if i == numberOfStaff() - 1 {
                NSLayoutConstraint.activate([
                    column.rightAnchor.constraint(equalTo: scrollViewCalender.rightAnchor)
                ])
            }
            createColumnHeader(index: i, width: Dimension.withColumn, height: 50, previewView: &previewHeader)
            
            preview = column
        }
        
    }
    
    private func createColumnHeader(index: Int, width: CGFloat, height: CGFloat, previewView: inout RowAvatarView) {
       
        let column = RowAvatarView()
        let name = titleNameOfStaff(rowIndex: index)
        let avatar = avatarOfStaff(rowIndex: index)
        column.setText(text: name, avatar: avatar)
        column.translatesAutoresizingMaskIntoConstraints = false
        
        scrollViewAvatar.addSubview(column)
        
        column.topAnchor.constraint(equalTo: scrollViewAvatar.topAnchor).isActive = true
        column.bottomAnchor.constraint(equalTo: scrollViewAvatar.bottomAnchor).isActive = true
        column.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        column.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        if index == 0 {
            column.leftAnchor.constraint(equalTo: scrollViewAvatar.leftAnchor).isActive = true
        } else {
            column.leftAnchor.constraint(equalTo: previewView.rightAnchor).isActive = true
        }
        
        if index == numberOfStaff() - 1 {
            column.rightAnchor.constraint(equalTo: scrollViewAvatar.rightAnchor).isActive = true
        }
        
        previewView = column
    }
    
    private func createColumnLeft(index: Int, previewView: inout UILabel){
        let column = RowTimeView()
        column.text = titleForLeftRow(rowIndex: index)
        
        column.translatesAutoresizingMaskIntoConstraints = false
        scrollViewTime.addSubview(column)
        column.heightAnchor.constraint(equalToConstant: Dimension.heightRow).isActive = true
        column.widthAnchor.constraint(equalToConstant: Dimension.widthTime).isActive = true
        column.leftAnchor.constraint(equalTo: scrollViewTime.leftAnchor).isActive = true
        column.rightAnchor.constraint(equalTo: scrollViewTime.rightAnchor).isActive = true
        if index == 0 {
            column.topAnchor.constraint(equalTo: scrollViewTime.topAnchor).isActive = true
        } else {
            column.topAnchor.constraint(equalTo: previewView.bottomAnchor).isActive = true
        }
        if index == numberOfStaff() - 1 {
            column.bottomAnchor.constraint(equalTo: scrollViewTime.bottomAnchor).isActive = true
        }
        previewView = column
    }
    
    func addBookingView(xPosition: CGFloat = 0, yPositon: CGFloat = 0, indexStaff: Int = 0 ,service: String) {
        let view = BookingView(frame: CGRect(x: xPosition, y: yPositon, width: Dimension.withColumn, height: Dimension.heightRow))
        view.backgroundColor = .randomBackground()
        view.set(parentView: scrollViewCalender)
        
        var time: CGFloat = 0
        time = (Dimension.heightRow + yPositon) / (Dimension.heightRow)
        
        let tt = minutesToHoursAndMinutes(420 + (Int(60 * time)))
     
        guard let itemStaff = getItemStaff(index: indexStaff) else {return}
        
        view.setText(string: "\(tt.hours):\(tt.leftMinutes)", staff: itemStaff)
        
        view.setTextService(serviceStr: service)
        view.delegate = self
        listBookingView.append(view)
        scrollViewCalender.addSubview(view)
    }
    
    
    // MARK: - get data to delegate
    
    private func numberOfStaff() -> Int {
        guard let data = dataSource else {
            return 0
        }
        return data.numberOfStaff()
    }
    
    private func numberOfRowInColumn(columnIndex: Int) -> Int {
        guard let d = dataSource else {return 0}
        return d.numberOfTime(in: columnIndex)
    }
    
   
    
    private func caculateHeightColumn(columnIndex: Int) -> CGFloat{
        return CGFloat((numberOfRowInColumn(columnIndex: columnIndex))) * Dimension.heightRow
    }
    
    private func titleForLeftRow(rowIndex: Int) -> String {
        guard let d = delegate else {
            return ""
        }
        return d.calenderView(titleForLeftRowAt: rowIndex)
    }
    
    private func titleNameOfStaff(rowIndex: Int) -> String {
        guard let d = delegate else {
            return ""
        }
        return d.calenderView(titleForNameStaff: rowIndex)
        
    }
    
    private func avatarOfStaff(rowIndex: Int) -> String {
        guard let d = delegate else {
            return ""
        }
        return d.calenderView(avatarForNameStaff: rowIndex)
        
    }
    
    private func showAlertSelectService() {
        guard let d = delegate else {
            return
        }
        d.calenderViewAlertSelectService()
    }
    
    private func getItemStaff(index: Int) -> StaffModel? {
        guard let d = delegate else {
            return nil
        }
        return d.calenderView(itemStaff: index)
    }
    
    func convertBinarySearch(widthItem: CGFloat) -> Int {
        var countCollum = offsetIndexCollumn.count
        var low = 0
        if low < countCollum {
            for i in 0..<countCollum {
                let mid = low + ((countCollum - low + 1) / 2)
                let currentOffset = offsetIndexCollumn[i]
                if currentOffset == widthItem {
                    return i
                } else if currentOffset < widthItem {
                    low = i
                } else {
                    countCollum = i - 1
                }
            }
        }
        return low
    }
    
   // MARK: - Action

    @objc func addLongPressBookingView(sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            let point = sender.location(in: self.scrollViewCalender)
            
            let indexX = Int(point.x) / 100
            let valueX = offsetIndexCollumn[indexX]
            
            let indexY = Int(point.y) / (150 / 5)
            let valueY = offSetYRowsAtTime[indexY]
            
            addBookingView(xPosition: valueX, yPositon: valueY, indexStaff: indexX, service: "Massage nuru")
        }
    }
    
    @objc func addBookingView(_ sender: UIButton) {
        showAlertSelectService()
    }

}

extension CalenderView: BookingViewDelegate {
    
    func viewWillMove(view: BookingView) {
        self.scrollViewCalender.isScrollEnabled = false
    }
    
    func viewDidMove(view: BookingView, location: CGPoint, direction: BookingViewMoveDirection) {
    }
    
    func viewDidEndMove(view: BookingView) {
        let widthItem = view.frame.origin.x + (view.frame.width / 2)

        let x = convertBinarySearch(widthItem: widthItem)
       
        let index = Int((view.frame.origin.y / 30).rounded())
        let y = (offSetYRowsAtTime[index])
        
        view.updateX(x: offsetIndexCollumn[x])
        
        view.updateY(y: y)
        let indexStaff = Int(view.frame.origin.x / 100)
        var time: CGFloat = 0
        
        time = (Dimension.heightRow + view.frame.origin.y) / (Dimension.heightRow)
        
        let tt = minutesToHoursAndMinutes(420 + (Int(60 * time)))
        
        guard let itemStaff = getItemStaff(index: indexStaff) else {return}
        
        view.setText(string: "\(tt.hours):\(tt.leftMinutes)", staff: itemStaff)
        self.scrollViewCalender.isScrollEnabled = true
    }
}

extension CalenderView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollViewCalender == scrollView {
            if scrollView.contentOffset.x < 0 {
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
                
            }
            if scrollView.contentOffset.y < 0 {
                scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: 0)
            }
            scrollViewTime.contentOffset.y = scrollView.contentOffset.y
            scrollViewAvatar.contentOffset.x = scrollView.contentOffset.x
        }
    }
}

func minutesToHoursAndMinutes(_ minutes: Int) -> (hours: Int , leftMinutes: Int) {
    return (minutes / 60, (minutes % 60))
}
