//
//  RowCalenderView.swift
//  BootcamPart1
//
//  Created by TranHieu on 11/12/2023.
//

import Foundation
import UIKit

open class RowCalenderView: UIView {
    
    private var label = UILabel()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        addSubview(label)
        let height = frame.height / 5
        for i in 0..<6 {
            drawLineFromPoint(start: CGPoint(x: 0, y: Int(height) * i), toPoint: CGPoint(x: frame.width, y: height * CGFloat(i)), ofColor: .lightGray.withAlphaComponent(1), inView: self)
        }
        label.frame = self.bounds
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .center
        
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 1
    }
    func setTitle(string: String) {
        label.text = string
    }
}
