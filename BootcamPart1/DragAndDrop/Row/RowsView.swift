//
//  RowsView.swift
//  BootcamPart1
//
//  Created by TranHieu on 18/12/2023.
//

import Foundation
import UIKit
open class RowsView: UIView {
    
    private var columnIndex = 0
    private var numberOfRows = 0
    private var heightRow:CGFloat = 0
    
    convenience init(columnIndex: Int, numberOfRows: Int, heightRow: CGFloat) {
        self.init(frame: .zero)
        self.columnIndex = columnIndex
        self.numberOfRows = numberOfRows
        self.heightRow = heightRow
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        setupColumn()
    }
    
    private func setupColumn(){
        var preview: RowCalenderView!
        for i in 0..<numberOfRows{
            let row = RowCalenderView()
            row.backgroundColor = UIColor.white
            addSubview(row)
            row.translatesAutoresizingMaskIntoConstraints = false
            
            row.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
            row.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
            row.heightAnchor.constraint(equalToConstant: heightRow).isActive = true
            if i == 0 {
                row.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            } else {
                row.topAnchor.constraint(equalTo: preview.bottomAnchor).isActive = true
            }
            if i == numberOfRows - 1 {
                row.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
            }
            preview = row
        }
    }
}
