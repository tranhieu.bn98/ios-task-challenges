//
//  RowAvatarView.swift
//  BootcamPart1
//
//  Created by TranHieu on 11/12/2023.
//

import Foundation
import UIKit

open class RowAvatarView: UIView {
    private var lbTitle: UILabel = UILabel()
    private var imgAvatar: UIImageView = UIImageView()
    
    private var centerView: UIView = UIView()
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
        lbTitle.font = UIFont.systemFont(ofSize: 12)
        lbTitle.textAlignment = .center
        
        centerView.translatesAutoresizingMaskIntoConstraints = false
        imgAvatar.translatesAutoresizingMaskIntoConstraints = false
        lbTitle.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(centerView)
        centerView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        centerView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        
        
        centerView.addSubview(imgAvatar)
        imgAvatar.topAnchor.constraint(equalTo: centerView.topAnchor).isActive = true
       
        imgAvatar.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgAvatar.heightAnchor.constraint(equalToConstant: 25).isActive = true
        imgAvatar.centerXAnchor.constraint(equalTo: centerView.centerXAnchor).isActive = true
        
       
        
        centerView.addSubview(lbTitle)
        lbTitle.topAnchor.constraint(equalTo: imgAvatar.bottomAnchor, constant: 5).isActive = true
        lbTitle.leftAnchor.constraint(equalTo: centerView.leftAnchor).isActive = true
        lbTitle.rightAnchor.constraint(equalTo: centerView.rightAnchor).isActive = true
        lbTitle.heightAnchor.constraint(equalToConstant: 10).isActive = true
        lbTitle.bottomAnchor.constraint(equalTo: centerView.bottomAnchor).isActive = true
    }
    func setText(text: String, avatar: String) {
        lbTitle.text = text
        imgAvatar.image = UIImage(named: avatar)
        imgAvatar.clipsToBounds = true
        imgAvatar.layer.cornerRadius = 12
    }
    
}
 
