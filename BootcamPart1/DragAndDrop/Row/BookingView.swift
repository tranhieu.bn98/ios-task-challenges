//
//  BookingView.swift
//  BootcamPart1
//
//  Created by TranHieu on 18/12/2023.
//

import Foundation
import UIKit

enum BookingViewMoveDirection {
    case left
    case right
    case none
}
protocol BookingViewDelegate{
    func viewWillMove(view: BookingView)
    func viewDidMove(view: BookingView, location: CGPoint, direction: BookingViewMoveDirection)
    func viewDidEndMove(view: BookingView)
}

class BookingView: UIView {
    private var parentView: UIView!
    private var currentY: CGFloat = 0
    private var currentX: CGFloat = 0
    private var timer: Timer!
    private var label: UILabel = UILabel()
    private var name: UILabel = UILabel()
    private var service: UILabel = UILabel()
    
    
    var delegate: BookingViewDelegate?
    var moveDirection: BookingViewMoveDirection = .none
    var isActive: Bool = false
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        isMultipleTouchEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        name.translatesAutoresizingMaskIntoConstraints = false
        service.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        addSubview(name)
        addSubview(service)
        self.alpha = 0.8
        
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            label.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        label.font = UIFont.systemFont(ofSize: 10)
        label.textAlignment = .center
        name.textAlignment = .center
        name.font = UIFont.systemFont(ofSize: 12)
        name.numberOfLines = 0
        
        NSLayoutConstraint.activate([
            name.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            name.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            name.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
        ])
        service.font = UIFont.systemFont(ofSize: 14)
        service.textAlignment = .center
        service.numberOfLines = 0
        NSLayoutConstraint.activate([
            service.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 5),
            service.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            service.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            service.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor, constant: 0)
        ])
        
        
        
    }
    private func startTimer(){
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false, block: { _  in
            UIView.animate(withDuration: 0.3) {
                self.alpha = 0.6
            } completion: { _ in
                self.isActive = true
                
            }
        })
    }
    
    private func stopTimer(){
        guard let t = timer else {return}
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0.8
            
        }
        t.invalidate()
        timer = nil
        isActive = false
        
        
    }
    
    func set(parentView: UIView) {
        self.parentView = parentView
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("touchesBegan")
        startTimer()
        guard let delegate = delegate else {
            return
        }
        parentView.bringSubviewToFront(self)
        delegate.viewWillMove(view: self)
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("touchesEnded")
        stopTimer()
        guard let delegate = delegate else {
            return
        }
        delegate.viewDidEndMove(view: self)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isActive {
            let touch = touches.first
            let location = touch?.location(in: parentView)
            
            if currentX > touch!.location(in: self).x {
                moveDirection = .left
            } else {
                moveDirection = .right
            }
            self.frame.origin.x = location!.x - (self.frame.width / 2)
            self.frame.origin.y = location!.y - (self.frame.height / 2)
            currentX = touch!.location(in: self).x
            guard let delegate = delegate, let location = location else {
                return
            }
            delegate.viewDidMove(view: self, location: location, direction: self.moveDirection)
        }
        
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("touchesCancelled")
    }
    override func touchesEstimatedPropertiesUpdated(_ touches: Set<UITouch>) {

    }
    
    func setTextService(serviceStr: String) {
        service.text = serviceStr
    }
    
    func setText(string:String, staff: StaffModel){
        label.text = string
        name.text = "\(staff.name)"
    }
    
    
    func updateX(x: CGFloat){
        self.frame.origin.x = x
    }
    
    func updateY(y: CGFloat) {
        self.frame.origin.y = y
    }
    
}
