//
//  RowTimeView.swift
//  BootcamPart1
//
//  Created by TranHieu on 11/12/2023.
//

import Foundation
import UIKit

open class RowTimeView: UILabel {
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
        self.font = UIFont.systemFont(ofSize: 12)
        self.textAlignment = .center
    }
    
}
