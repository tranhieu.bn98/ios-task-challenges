//
//  Extension.swift
//  BootcamPart1
//
//  Created by TranHieu on 18/12/2023.
//

import Foundation
import UIKit

extension Collection where Element: Comparable, Index == Int {
    func binarySearch(_ element: Element) -> Index {
        var low = 0
        var high = count - 1
        
        while low < high {
            let mid = low + ((high - low + 1) / 2)
           
            let current = self[mid]
            if current == element {
                return mid
            } else if current < element {
                low = mid
            } else {
                high = mid - 1
            }
        }
        return low
    }
}
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func randomBackground() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
           alpha: 1.0
        )
    }
}
