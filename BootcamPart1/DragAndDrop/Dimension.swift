//
//  Dimension.swift
//  BootcamPart1
//
//  Created by TranHieu on 11/12/2023.
//

import Foundation
import UIKit

struct Dimension {
    
    static var heightHeader : CGFloat = 60
    static var widthTime    : CGFloat = 50
    static var heightAvatar : CGFloat = 50
    static var withColumn   : CGFloat = 100
    static var heightRow    : CGFloat = 150
    
    static var dimensionButton: CGFloat = 50
    
}

func drawLineFromPoint(start : CGPoint, toPoint end:CGPoint, ofColor lineColor: UIColor, inView view:UIView) {
    //design the path
    let path = UIBezierPath()
    path.move(to: start)
    path.addLine(to: end)

    //design path in layer
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = path.cgPath
    shapeLayer.strokeColor = lineColor.cgColor
    shapeLayer.lineWidth = 1.0

    view.layer.addSublayer(shapeLayer)
}
