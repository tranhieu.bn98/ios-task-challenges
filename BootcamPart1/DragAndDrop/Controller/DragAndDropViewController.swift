//
//  DragAndDropViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 08/12/2023.
//

import UIKit

class DragAndDropViewController: UIViewController {
    let calenderView = CalenderView()
    let dataRows = WorkModel.fakeData()
    var dataStaff: [StaffModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Booking Massage"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        view.addSubview(calenderView)
        dataStaff = StaffModel.fakeData()
        calenderView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            calenderView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            calenderView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            calenderView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            calenderView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])

        calenderView.listWorkModel = dataRows
        calenderView.delegate = self
        calenderView.dataSource = self
        calenderView.reloadData()
       
    }
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        if parent == nil {
            calenderView.countdownTimer?.invalidate()
        }
    }
    @objc func addTapped() {
        let item = StaffModel(name: "Staff_\(dataStaff.count)", avatar: "girl_3")
        dataStaff.append(item)
       
        calenderView.reloadItem()
        
        calenderView.reloadData()
        calenderView.reloadBookingView()
    }

}
extension DragAndDropViewController: CalenderViewDataSource, CalenderViewDelegate {
    func calenderView(itemStaff index: Int) -> StaffModel {
        return dataStaff[index]
    }
    
    func numberOfStaff() -> Int {
        return dataStaff.count
    }
    
    func numberOfTime(in column: Int) -> Int {
        return dataRows.count
    }
    
    
    func calenderViewAlertSelectService() {
        let alert = UIAlertController(title: "Chú ý!", message: "Xin mời chọn dịch vụ Massage", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Massage nuru", style: .default, handler: { action in
        
                self.calenderView.addBookingView(service: "Massage nuru")
        }))
        
        alert.addAction(UIAlertAction(title: "Massage lingam", style: .default, handler: { action in
        
            self.calenderView.addBookingView(service: "Massage lingam")
        }))
        
        alert.addAction(UIAlertAction(title: "Massage body", style: .default, handler: { action in
        
            self.calenderView.addBookingView(service: "Massage body")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    func calenderView(titleForNameStaff index: Int) -> String {
        return dataStaff[index].name
    }
    
    func calenderView(avatarForNameStaff index: Int) -> String {
        return dataStaff[index].avatar
    }

    func calenderView(titleForLeftRowAt index: Int) -> String {
        return dataRows[index].time
    }
    
    func calenderView(column: RowsView, didSelectedColumnAt columnIndex: Int) {
    }
}
