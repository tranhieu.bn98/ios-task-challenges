//
//  ItemPokemonCell.swift
//  BootcamPart1
//
//  Created by TranHieu on 17/01/2024.
//

import UIKit

class ItemPokemonCell: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    
    @IBOutlet weak var lbPosition: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func updateUI(item: ARItem) {
        lbName.text = item.iconMaker
        lbPosition.text = "Latitude: \(item.location.coordinate.latitude) \nLongitude: \(item.location.coordinate.longitude)"
    }
}
