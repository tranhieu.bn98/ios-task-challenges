//
//  AddPokemonViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 22/12/2023.
//

import UIKit
import CoreLocation

class AddPokemonViewController: UIViewController {
    
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var tabPokemonView: UITableView!
    
    @IBOutlet weak var lbEmpty: UILabel!
    
    var latDouble: Double = 0.0
    var logDouble: Double = 0.0
    var namePokemon: String = ""
    var roomTextField: UITextField!
    
    var targets: [ARItem] = []
    
    deinit {
        latDouble = 0.0
        logDouble = 0.0
        namePokemon = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        btnAdd.layer.cornerRadius = 6
        tabPokemonView.register(UINib(nibName: "ItemPokemonCell", bundle: nil), forCellReuseIdentifier: "ItemPokemonCell")
        tabPokemonView.delegate = self
        tabPokemonView.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    func goToMapPokemonViewController(latitude: Double, longitude: Double) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "MapPokemonViewController", bundle:nil)
        let viewControllerB = storyBoard.instantiateViewController(withIdentifier: "MapPokemonViewController") as! MapPokemonViewController
        
      //  let item = ARItem(iconMaker: "ic_treasure_chest", itemDescription: "dragon", location: CLLocation(latitude: latitude, longitude: longitude), itemNode: nil)
        
        viewControllerB.targets = self.targets
        self.navigationController?.pushViewController(viewControllerB, animated: true)
    }
    
    func alertControllerWithTf() {
        let dialogMessage = UIAlertController(title: "New Room", message: "Latitude: 100 \n Longitude: 200", preferredStyle: .alert)
        let label = UILabel(frame: CGRect(x: 0, y: 40, width: 270, height:18))
        label.textAlignment = .center
        label.textColor = .red
        label.font = label.font.withSize(12)
        dialogMessage.view.addSubview(label)
        label.isHidden = true
        
        let Create = UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
            print("Create button success block called do stuff here....")
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) -> Void in
            print("Cancel button tapped")
        }
        
        //Add OK and Cancel button to dialog message
        
        dialogMessage.addAction(Create)
        dialogMessage.addAction(cancel)
        // Add Input TextField to dialog message
        dialogMessage.addTextField { (textField) -> Void in
            self.roomTextField = textField
            self.roomTextField?.placeholder = "Please enter room name"
        }
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @objc private func addTapped() {
        
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        view.endEditing(true)
        goToMapPokemonViewController(latitude: latDouble, longitude: logDouble)
        
    }
    
    @IBAction func actionSelectPlaceInMap(_ sender: Any) {
        view.endEditing(true)
        let storyBoard : UIStoryboard = UIStoryboard(name: "MapPlaceViewController", bundle:nil)
        let viewControllerB = storyBoard.instantiateViewController(withIdentifier: "MapPlaceViewController") as! MapPlaceViewController
        viewControllerB.delegate = self
        
        self.navigationController?.pushViewController(viewControllerB, animated: true)
        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension AddPokemonViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return targets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemPokemonCell", for: indexPath) as! ItemPokemonCell
        let item = targets[indexPath.row]
        cell.updateUI(item: item)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension AddPokemonViewController: MapPlaceViewControllerDelegate {
    func positionSelectInMap(targets: [ARItem]) {
        self.lbEmpty.isHidden = targets.count > 0
        self.btnAdd.isHidden = !(targets.count > 0)
        self.targets = targets
        self.tabPokemonView.reloadData()
    }
    
    func positionSelectInMap(coordinate: CLLocationCoordinate2D) {
        latDouble = coordinate.latitude
        logDouble = coordinate.longitude
        //        lbLat.text = "Latitude: \(coordinate.latitude)"
        //        lbLog.text = "Longitude: \(coordinate.longitude)"
    }
    
}
