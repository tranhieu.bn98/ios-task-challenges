//
//  MapPokemonViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 21/12/2023.
//

import UIKit
import MapKit
import GoogleMaps
import SceneKit
import CoreLocation
import AVFoundation
import GoogleMaps
import CoreLocation
import SceneKit
import ARKit

class MapPokemonViewController: UIViewController {
    
    @IBOutlet weak var viewContainMap: UIView!
    
    @IBOutlet weak var viewContainCamera: UIView!
    
    @IBOutlet weak var heightViewContainCamera: NSLayoutConstraint!
    
    @IBOutlet weak var sceneView: ARSKView!
    
    //MARK: - UI objects Outlets
    var mapView: GMSMapView!
    
    //MARK: - Location variables
    var locationManager = CLLocationManager()
    var userLocation = CLLocation()
    
    //MARK: - Map camera variables
    var bearingAngle = 270.0
    var angleOfView = 65.0
    var zoomLevel:Float = 18.5
    var capitolLat = 38.889815
    var capitolLon = -77.005900
    
    //MARK: - Custom user marker variables
    var userMarker = GMSMarker()
    let userMarkerimageView = UIImageView(image: UIImage(named: "ic_person"))
    //MARK: - Scoring Variables
    var diamond1Score = 00
    let defaultLocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
    
    
    
    var cameraSession: AVCaptureSession?
    var cameraLayer: AVCaptureVideoPreviewLayer?
    var target: ARItem!
    var heading: Double = 0
    
    var isSetupArAnchor: Bool = false
    var targets: [ARItem] = []
    var distanceMinItem = 40.0
    
    var isTargetItem: Bool = false
    
    var listMarkers = [GMSMarker]()
    
    var scene = PokemonScene()
    
    var isGetFirstHeading: Bool = false
    
    var indexNode = -1
    
    //MARK: - Main Function
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        initializeMap()
        
        loadCamera()
        self.cameraSession?.startRunning()
        self.locationManager.headingFilter = 5
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and node count
        sceneView.showsFPS = true
        sceneView.showsNodeCount = true
        
        scene  = PokemonScene(size: sceneView.bounds.size)
        scene.scaleMode = .resizeFill
        
        sceneView.presentScene(scene)
        
        scene.callbackArraySKNode = { [weak self] node in
            let fadeOut = SKAction.fadeOut(withDuration: 0.5)
            let remove = SKAction.removeFromParent()
            
            // Group the fade out and sound actions
            let groupKillingActions = SKAction.group([fadeOut])
            // Create an action sequence
            let sequenceAction = SKAction.sequence([groupKillingActions, remove])
            self?.targets.forEach { item in
                if item.iconMaker == node.name {
                    item.marker?.map = nil
                }
            }
            node.run(sequenceAction)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        //configuration.planeDetection = .horizontal
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // - MARK: - Camera
    func createCaptureSession() -> (session: AVCaptureSession?, error: NSError?) {
        var error: NSError?
        var captureSession: AVCaptureSession?
        
        let backVideoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
        
        if backVideoDevice != nil {
            var videoInput: AVCaptureDeviceInput!
            do {
                videoInput = try AVCaptureDeviceInput(device: backVideoDevice!)
            } catch let error1 as NSError {
                error = error1
                videoInput = nil
            }
            
            if error == nil {
                captureSession = AVCaptureSession()
                
                if captureSession!.canAddInput(videoInput) {
                    captureSession!.addInput(videoInput)
                } else {
                    error = NSError(domain: "", code: 0, userInfo: ["description": "Error adding video input."])
                }
            } else {
                error = NSError(domain: "", code: 1, userInfo: ["description": "Error creating capture device input."])
            }
        } else {
            error = NSError(domain: "", code: 2, userInfo: ["description": "Back video device not found."])
        }
        
        return (session: captureSession, error: error)
    }
    
    func setUpDataARAnchor() {
        for i in 0..<targets.count {
            let heading = getHeadingForDirectionFromCoordinate(from: userLocation, to: targets[i].location)
            let delta = heading - self.heading
            let arAnchor = convertArAnchor(heading: delta)
            targets[i].arAnchor = arAnchor
        }
        addMarkers()
        repositionTarget()
        isSetupArAnchor = true
        
    }
    
    func loadCamera() {
        let captureSessionResult = createCaptureSession()
        guard captureSessionResult.error == nil, let session = captureSessionResult.session else {
            print("Error creating capture session")
            return
        }
        
        self.cameraSession = session
        let cameraLayer = AVCaptureVideoPreviewLayer(session: self.cameraSession!)
        
        cameraLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraLayer.frame = self.view.bounds
        self.viewContainCamera.layer.insertSublayer(cameraLayer, at: 0)
        self.cameraLayer = cameraLayer
        
    }
    
    // - MARK: - SceneKit
    func repositionTarget() {
        targets.forEach { item in
            
            if let ar = item.arAnchor {
                scene.createGhostAnchor(anchor: ar)
            }
            //            let distance = userLocation.distance(from: item.location)
            //            if distance <= 5 {
            
            //            } else {
            //                if let ar = item.arAnchor {
            //                    scene.removeGhostAnchor(anchor: ar)
            //                }
            //            }
        }
    }
    
    func randomFloat(min: Float, max: Float) -> Float {
        return (Float(arc4random()) / 0xFFFFFFFF) * (max - min) + min
    }
    
    func convertArAnchor(heading: Double) -> ARAnchor {
        // Define 360º in radians
        let _360degrees = 2.0 * Float.pi
        
        let r = 1 - (heading / 360)//randomFloat(min: 0.0, max: 1.0)
        let rotateX = simd_float4x4(SCNMatrix4MakeRotation(_360degrees * Float(1), 1, 0, 0))
        let rotateY = simd_float4x4(SCNMatrix4MakeRotation(_360degrees * Float(r), 0, 1, 0))
        
        let rotation = simd_mul(rotateX, rotateY)
        
        // Create a translation matrix in the Z-axis with a value between 1 and 2 meters
        var translation = matrix_identity_float4x4
        translation.columns.3.z = -1 - randomFloat(min: 0.0, max: 1.0)
        
        // Combine the rotation and translation matrices
        let transform = simd_mul(rotation, translation)
        // Create an anchor
        let anchor = ARAnchor(transform: transform)
        return anchor
    }
    
    func radiansToDegrees(_ radians: Double) -> Double {
        return (radians) * (180.0 / .pi)
    }
    
    func degreesToRadians(_ degrees: Double) -> Double {
        return (degrees) * (.pi / 180.0)
    }
    
    func getHeadingForDirectionFromCoordinate(from: CLLocation, to: CLLocation) -> Double {
        let fLat = degreesToRadians(from.coordinate.latitude)
        let fLng = degreesToRadians(from.coordinate.longitude)
        
        let tLat = degreesToRadians(to.coordinate.latitude)
        let tLng = degreesToRadians(to.coordinate.longitude)
        
        let degree = radiansToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)))
        
        if degree >= 0 {
            return degree
        } else {
            return degree + 360
        }
    }
    
    func centerMapAtUserLocation() {
        let locationObj = locationManager.location
        let coord = locationObj?.coordinate
        let lattitude = coord?.latitude
        let longitude = coord?.longitude
        userMarkerimageView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lattitude ?? capitolLat, longitude: longitude ?? capitolLon, zoom: zoomLevel, bearing: bearingAngle, viewingAngle: angleOfView)
        self.mapView.animate(to: camera)
        
    }
    
    func checkUserPermission() {
        locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                perform(#selector(presentNotDeterminedPopup), with: nil, afterDelay: 0)
            case .restricted, .denied:
                perform(#selector(presentDeniedPopup), with: nil, afterDelay: 0)
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.startUpdatingLocation()
                locationManager.startUpdatingHeading()
                centerMapAtUserLocation() //step 3
            @unknown default:
                print("")
            }
        } else {
            perform(#selector(presentDeniedPopup), with: nil, afterDelay: 0)
        }
    }
    
    @objc private func presentNotDeterminedPopup() {
        
        let title = "Allow Location"
        let message = "Allow location to discover and collect diamonds near you."
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            print("OKKKKKK")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func presentDeniedPopup() {
        
        let title = "Allow Location"
        let message = "Allow location to discover and collect diamonds near you. Open setting and allow location when in use."
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            print("OKKKKKK")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - All methods related to map combined
    func initializeMap() {
        let camera = GMSCameraPosition.camera(withLatitude: capitolLat, longitude: capitolLon, zoom: zoomLevel, bearing: bearingAngle,
                                              viewingAngle: angleOfView)
        mapView = GMSMapView.map(withFrame: viewContainMap.bounds, camera: camera)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.settings.tiltGestures = false
        mapView.settings.rotateGestures = false
        mapView.settings.zoomGestures = false
        mapView.settings.compassButton = true
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        mapView.settings.indoorPicker = false
        mapView.isMyLocationEnabled = true
        mapView.isBuildingsEnabled = false
        self.mapView.settings.scrollGestures = false
        self.mapView.delegate = self
        
        checkUserPermission() //step 2
        viewContainMap.addSubview(mapView)
    }
    
    func distanceInMeters(marker: GMSMarker) -> CLLocationDistance {
        let markerLocation = CLLocation(latitude: marker.position.latitude , longitude: marker.position.longitude)
        let metres = locationManager.location?.distance(from: markerLocation)
        return Double(metres ?? -1)
    }
    
    func addMarkers() {
        for i in 0..<targets.count {
            var marker:  GMSMarker?
            let position = CLLocationCoordinate2D(latitude: targets[i].location.coordinate.latitude, longitude: targets[i].location.coordinate.longitude)
            marker = GMSMarker(position: position)
            
            marker?.title = "title: \(targets[i].iconMaker)"
            marker?.userData = ["index": i]
            let diamond1Gif = UIImage(named: "ic_pokemon_ball")
            
            let diamond1GifView = UIImageView(image: diamond1Gif)
            diamond1GifView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            marker?.map = mapView
            marker?.iconView = diamond1GifView
            
            if let m = marker {
                targets[i].marker = m
            }
        }
    }
    
    func randomInt(min: Int, max: Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    @IBAction func actionBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension MapPokemonViewController: GMSMapViewDelegate {
    //MARK: - Map Markers Delegate method
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("marker \(marker.title ?? "")")
        return true
        
    }
}

extension MapPokemonViewController: CLLocationManagerDelegate {
    //MARK: - Location permission Delegate Method
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            perform(#selector(presentNotDeterminedPopup), with: nil, afterDelay: 0)
        case .restricted, .denied:
            perform(#selector(presentDeniedPopup), with: nil, afterDelay: 0)
        case .authorizedAlways, .authorizedWhenInUse:
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            self.centerMapAtUserLocation() //step 3
        @unknown default:
            print("error")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations.last ?? CLLocation(latitude: capitolLat, longitude: capitolLon)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        DispatchQueue.main.async {
            self.heading = fmod(newHeading.trueHeading, 360.0)
            self.mapView.animate(toBearing: newHeading.magneticHeading)
            if !self.isSetupArAnchor {
                self.setUpDataARAnchor()
            }
        }
    }
}

extension MapPokemonViewController: ARSKViewDelegate {
    
    func view(_ view: ARSKView, nodeFor anchor: ARAnchor) -> SKNode? {
        let ghostId = randomInt(min: 1, max: 6)
        indexNode += 1
        let node = SKSpriteNode(imageNamed: "ghost\(ghostId)")
        node.name = targets[indexNode].iconMaker
        return node
    }
}
