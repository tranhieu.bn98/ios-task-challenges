//
//  MapPlaceViewController.swift
//  BootcamPart1
//
//  Created by TranHieu on 11/01/2024.
//

import UIKit
import GoogleMaps
import CoreLocation
protocol MapPlaceViewControllerDelegate: AnyObject {
    func positionSelectInMap(targets: [ARItem])
}

class MapPlaceViewController: UIViewController {
    
    var delegate: MapPlaceViewControllerDelegate!
    
    var mapView: GMSMapView!
    
    private let buttonBack = UIButton()

    var locationManager = CLLocationManager()
    var userLocation = CLLocation()
    var bearingAngle = 0.0
    var angleOfView = 0.0
    var zoomLevel:Float = 16
    var capitolLat = 21.009441760137182
    var capitolLon = 105.85522715002298
    
    var targets: [ARItem] = []
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        initializeMap()
    }
    
    func initializeMap() {
        let camera = GMSCameraPosition.camera(withLatitude: capitolLat, longitude: capitolLon, zoom: zoomLevel, bearing: bearingAngle,
                                              viewingAngle: angleOfView)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mapView.settings.tiltGestures = true
        self.mapView.settings.rotateGestures = true
        self.mapView.settings.zoomGestures = true
        self.mapView.settings.compassButton = true
        self.mapView.settings.myLocationButton = true
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        
        mapView.settings.indoorPicker = false
        mapView.isUserInteractionEnabled = true
        mapView.isMyLocationEnabled = true
        
        self.mapView.settings.scrollGestures = true
       self.mapView.delegate = self
        
        checkUserPermission() //step 2
        view.addSubview(mapView)
        
        buttonBack.setTitle("Done", for: .normal)
        buttonBack.setTitleColor(UIColor.systemBlue, for: .normal)
        
        buttonBack.addTarget(self, action: #selector(actionDone), for: .touchUpInside)
       

        buttonBack.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonBack)
        
        NSLayoutConstraint.activate([
            buttonBack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            buttonBack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            buttonBack.heightAnchor.constraint(equalToConstant: 40),
            buttonBack.widthAnchor.constraint(equalToConstant: 60)
        ])
        
    }
    
    func checkUserPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                perform(#selector(presentNotDeterminedPopup), with: nil, afterDelay: 0)
            case .restricted, .denied:
                perform(#selector(presentDeniedPopup), with: nil, afterDelay: 0)
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.startUpdatingLocation()
                locationManager.startUpdatingHeading()
            }
        } else {
            perform(#selector(presentDeniedPopup), with: nil, afterDelay: 0)
        }
    }
    
    func addMakerUserSelect(latDouble: Double, logDouble: Double, name: String) {
        var marker:  GMSMarker?
        let position = CLLocationCoordinate2D(latitude: latDouble, longitude: logDouble)
        marker = GMSMarker(position: position)
        
        marker?.title = name
      
        let diamond1Gif = UIImage(named: "ic_marker")
        
        let diamond1GifView = UIImageView(image: diamond1Gif)
        diamond1GifView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        marker?.map = mapView
        marker?.iconView = diamond1GifView
        
        let itemPokemon = ARItem(iconMaker: name, itemDescription: "dragon", location: CLLocation(latitude: latDouble, longitude: logDouble), itemNode: nil)
        targets.append(itemPokemon)
    }
    
    func alertControllerWithTf(latDouble: Double, logDouble: Double) {
        let dialogMessage = UIAlertController(title: "Add Pokemon", message: "Latitude: \(latDouble) \n Longitude: \(logDouble)", preferredStyle: .alert)
        let label = UILabel(frame: CGRect(x: 0, y: 40, width: 270, height:18))
        label.textAlignment = .center
        label.textColor = .red
        label.font = label.font.withSize(12)
        dialogMessage.view.addSubview(label)
        label.isHidden = true

        let Create = UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
            print("Create button success block called do stuff here....")
            let text = dialogMessage.textFields?.last?.text ?? "nil"
            self.addMakerUserSelect(latDouble: latDouble, logDouble: logDouble, name: text)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) -> Void in
            print("Cancel button tapped")
        }

        dialogMessage.addAction(Create)
        dialogMessage.addAction(cancel)
        
        dialogMessage.addTextField { (textField) -> Void in
            print("")
        }
       
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @objc private func actionDone(sender: UIButton){
        delegate.positionSelectInMap(targets: self.targets)
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func presentNotDeterminedPopup() {
        
        //        let title = "Allow Location"
        //        let message = "Allow location to discover and collect diamonds near you."
    }
    
    @objc private func presentDeniedPopup() {
        
        //        let title = "Allow Location"
        //        let message = "Allow location to discover and collect diamonds near you. Open setting and allow location when in use."
    }
}

extension MapPlaceViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let latDouble = coordinate.latitude
        let logDouble = coordinate.longitude
        alertControllerWithTf(latDouble: latDouble, logDouble: logDouble)
    }

}
