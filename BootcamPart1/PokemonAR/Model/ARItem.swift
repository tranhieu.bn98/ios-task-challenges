//
//  ARItem.swift
//  BootcamPart1
//
//  Created by TranHieu on 21/12/2023.
//

import Foundation
import CoreLocation
import SceneKit
import ARKit
import GoogleMaps

struct ARItem {
    let iconMaker: String
    let itemDescription: String
    let location: CLLocation
    var itemNode: SCNNode?
    var isSkipFight: Bool = false
    var arAnchor: ARAnchor?
    var isAdd: Bool = false
    var marker: GMSMarker?
}
