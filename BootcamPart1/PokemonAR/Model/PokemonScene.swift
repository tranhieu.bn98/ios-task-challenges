//
//  PokemonScene.swift
//  BootcamPart1
//
//  Created by TranHieu on 22/01/2024.
//

import SpriteKit
import ARKit

class PokemonScene: SKScene {
    
    var targets: [ARItem] = []
    
    var callbackArraySKNode: ((_ sknode: SKNode) -> Void)?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Get the first touch
        guard let touch = touches.first else {
            return
        }
        // Get the location in the AR scene
        let location = touch.location(in: self)
        
        // Get the nodes at that location
        let hit = nodes(at: location)
        //  Get the first node (if any)
        
        if let node = hit.first {
            callbackArraySKNode?(node)
        }
    }
    
    func createGhostAnchor(anchor: ARAnchor) {
        guard let sceneView = self.view as? ARSKView else {
            return
        }
        sceneView.session.add(anchor: anchor)
    }
    func removeGhostAnchor(anchor: ARAnchor) {
        guard let sceneView = self.view as? ARSKView else {
            return
        }
        sceneView.session.remove(anchor: anchor)
    }
}
